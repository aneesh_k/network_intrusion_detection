FROM kulkarniane/refragment

MAINTAINER Aneesh Kulkarni

LABEL description="NIDS"

WORKDIR /

RUN cp /home/ip_tcp_refragment.py /

ENTRYPOINT ["python", "ip_tcp_refragment.py"]

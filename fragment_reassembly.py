from scapy.all import *
import ipaddress
from io import BytesIO
import yaml
import binascii

data = dict()
time_stamp = dict()
seq_table = dict()
ack = dict()
ip_addr_src=dict()
ip_addr_dst=dict()
port_src = dict()
port_dst=dict()

# Function for reassembling IP streams using 'last'


def ip_last(fragmentsin, expr, expr_port, expr_name):
    ip_buffer = BytesIO()
    data = [None] * 100000
    for pkt in fragmentsin:
        ip_buffer.seek(pkt[IP].frag * 8)
        ip_buffer.write(pkt[Raw].load)
        last_buff = ip_buffer.getvalue()[8:]
        for match in re.finditer(expr, str(last_buff)):
            time = int(match.start())
            out = {"timestamp": pkt.time,
               "source": {"ipv4_address": pkt[IP].src, "tcp_port": ""},
               "target": {"ipv4_address": pkt[IP].src, "tcp_port": ""}, "rule": expr_name}
            print(yaml.dump(out, default_flow_style=False))
    return ip_buffer.getvalue()[8:]


# Function for reassembling IP streams using 'first'


def ip_first(fragmentsin, expr, expr_port, expr_name):
    ip_buffer = BytesIO()
    for pkt in fragmentsin[::-1]:
        ip_buffer.seek(pkt[IP].frag * 8)
        ip_buffer.write(pkt[Raw].load)
        first_buff = ip_buffer.getvalue()[8:]
        for match in re.finditer(expr, str(first_buff)):
            time = int(match.start())
            out = {"timestamp": pkt.time,
               "source": {"ipv4_address": pkt[IP].src, "tcp_port": ""},
               "target": {"ipv4_address": pkt[IP].src, "tcp_port": ""}, "rule": expr_name}
            print(yaml.dump(out, default_flow_style=False))
    return ip_buffer.getvalue()[8:]


# Function for reassembling IP streams using 'linux'

def ip_linux(fragmentsin, check, expr, expr_port, expr_name):
    ip_buffer = BytesIO()
    for pkt in sorted(fragmentsin, key=lambda x: x[IP].frag, reverse=True):
        ip_buffer.seek(pkt[IP].frag * 8)
        ip_buffer.write(pkt[Raw].load)
        linux_buff = ip_buffer.getvalue()[8:]
        for match in re.finditer(expr, str(linux_buff)):
            time = int(match.start())
            out = {"timestamp": pkt.time,
               "source": {"ipv4_address": pkt[IP].src, "tcp_port": ""},
               "target": {"ipv4_address": pkt[IP].src, "tcp_port": ""}, "rule": expr_name}
            print(yaml.dump(out, default_flow_style=False))
    return ip_buffer.getvalue()[8:]

# Function for changing keys of sessions to keys of strings of values


def sort_keys_to_list(p):
    sort_key = None
    if 'IP' in p:
        if 'TCP' in p:
            sort_key = str(sorted(['TCP', str(p[IP].src), str(p[TCP].sport), str(p[IP].dst), str(p[TCP].dport)], key=str))
        elif 'ICMP' in p:
            sort_key = str(sorted(['ICMP', str(p[IP].src), str(p[IP].dst), str(p[ICMP].code), str(p[ICMP].type), str(p[ICMP].id)] ,key=str))
        else:
            sort_key = str(sorted(['IP', str(p[IP].src), str(p[IP].dst), str(p[IP].proto)] ,key=str))
    return sort_key


# Verify the IPs belong to the subnet under monitoring

def check_subnet(packet, subnet):
    if packet.haslayer(IP):
        source_ip = packet[IP].src
        dest_ip = packet[IP].dst
        if (ipaddress.ip_address(source_ip) in ipaddress.ip_network(subnet)) or (ipaddress.ip_address(dest_ip) in ipaddress.ip_network(subnet)):
            return True
        else:
            return False

# Checking the checksum of an individual packet


def check_checksum(pack):
    try:
        new_tcp_checksum = None
        new_ip_checksum = None
        ip_checksum = None
        tcp_checksum = None
        if i == 10:
            sys.exit(0)
        if IP in pack:
            ip_checksum = pack[IP].chksum
            del pack[IP].chksum
        if TCP in pack:
            tcp_checksum = pack[TCP].chksum
            del pack[TCP].chksum
        newip = pack.show2(dump=True)
        packet_values = newip.split()
        if 'chksum' in packet_values:
            index = packet_values.index('chksum')
            new_ip_checksum = packet_values[index+2]
            new_ip_checksum = (int(new_ip_checksum, 0))
            packet_values.remove('chksum')
        if 'chksum' in packet_values:
            index2 = packet_values.index('chksum')
            new_tcp_checksum = packet_values[index2 + 2]
            new_tcp_checksum = (int(new_tcp_checksum, 0))
            packet_values.remove('chksum')
        if new_ip_checksum == ip_checksum and new_tcp_checksum == tcp_checksum:
            return True
        elif new_ip_checksum == ip_checksum and tcp_checksum is None:
            return True
        elif new_tcp_checksum == tcp_checksum:
            return True
        else:
            return False
    except Exception:
        return False


'''
Sorted via lowest seq number which fills the data first. Overwrites existing data in the buffer.
'''


def first_overwrite(p, data_list, timestamp_list, data_offset, payload, ack, seq_list, check):
    count = 0
    flag = None
    data_offset = int(data_offset)
    payload = list((bytes(payload)).decode('utf-8'))
    payload_length = len(payload)
    if data_list[1] is None and ack == 0:
        ack = 0
    if ack > (int(data_offset) + payload_length):
        return data_list, timestamp_list, ack, seq_list
    for i in payload:
        if (data_list[data_offset] is None) and data_offset > ack:
            flag = data_offset
            break
        elif seq_list[data_offset] is not None and seq_list[data_offset] > p[TCP].seq and data_offset > ack:
            flag = data_offset
        data_offset += 1
        count += 1
    payload = payload[-count:]
    if flag is not None:
        for i in payload:
            # if data_list[flag] is None or seq_list[flag] > p[TCP].seq:
            data_list[flag] = i
            timestamp_list[flag] = p.time
            seq_list[flag] = p[TCP].seq
            flag += 1
    iter = 0
    while 1:
        if data_list[iter] is None:
            ack = iter
            break
        iter += 1

    return data_list, timestamp_list, ack, seq_list


'''
Sorted via lowest seq number which fills the data first. Does not overwrite existing data in the buffer.
'''


def first_no_overwrite(p, data_list, timestamp_list, data_offset, payload, ack, seq_list, check):
    count = 0
    flag = None
    data_offset = int(data_offset)
    payload = list((bytes(payload)).decode('utf-8'))
    payload_length = len(payload)
    if data_list[1] is None and ack == 0:
        ack = 0
    if ack > (int(data_offset) + payload_length):
        return data_list, timestamp_list, ack, seq_list
    for i in payload:
        if (data_list[data_offset] is None) and data_offset > ack:
            flag = data_offset
            break
        elif seq_list[data_offset] is not None and seq_list[data_offset] > p[TCP].seq and data_offset > ack:
            flag = data_offset
        data_offset += 1
        count += 1
    payload = payload[-count:]
    if flag is not None:
        for i in payload:
            if data_list[flag] is None:
                data_list[flag] = i
                timestamp_list[flag] = p.time
                seq_list[flag] = p[TCP].seq
            flag += 1
    iter = 0
    while 1:
        if data_list[iter] is None:
            ack = iter
            iter += 1
            break

    return data_list, timestamp_list, ack, seq_list


'''
Sorted via lowest seq number which fills the data last. Overwrites existing data in the buffer based on time and seq no.
'''


def last(p, data_list, timestamp_list, data_offset, payload, ack, seq_list, check):
    count = 0
    flag = None
    data_offset = int(data_offset)
    payload = list((bytes(payload)).decode('utf-8'))
    payload_length = len(payload)
    if data_list[1] is None and ack == 0:
        ack = 0
    if ack > (int(data_offset) + payload_length):
        return data_list, timestamp_list, ack, seq_list
    for i in payload:
        if (data_list[data_offset] is None) and data_offset > ack:
            flag = data_offset
            break
        elif (data_list[data_offset] is not None) and (seq_list[data_offset] > p[TCP].seq) and (timestamp_list[data_offset] <= p.time):
            flag = data_offset
            break
        data_offset += 1
        count += 1
    payload = payload[-count:]
    if flag is not None:
        for i in payload:  #
            if data_list[flag] is None:
                data_list[flag] = i
                timestamp_list[flag] = p.time
                seq_list[flag] = p[TCP].seq
            elif seq_list[flag] <= p[TCP].seq and timestamp_list[flag] <= p.time:
                data_list[flag] = i
                timestamp_list[flag] = p.time
                seq_list[flag] = p[TCP].seq
            flag += 1
    iter = 0
    while 1:
        if data_list[iter] is None:
            ack = iter
            iter += 1
            break

    return data_list, timestamp_list, ack, seq_list


def ip_reassembly(pcap_path, method, checksum, expr, expr_port, expr_name):
    packets = rdpcap(pcap_path)
    linux_fragments = dict()
    first_fragments = dict()
    last_fragments = dict()
    packetid = dict()

    packets = [a for a in packets if a.haslayer("IP")]
    fragmented_packets = [a for a in packets if a[IP].flags == 1 or a[IP].frag > 0]
    for a in fragmented_packets:
        packetid[a[IP].id] = [a[IP].src, a[IP].dst, a[IP].proto]

    for id in list(packetid.keys()):
        all_fragments = [a for a in fragmented_packets if a[IP].id == id and a.haslayer(Raw)]
        if str(method).lower() == "first":
            firstval = ip_first(all_fragments)
        elif str(method).lower() == "last":
            lastval = ip_last(all_fragments)
        elif str(method).lower() == "linux":
            linuxval = ip_linux(all_fragments, checksum)


def tcp_reassembly(pcap_path, method, checksum, expr, expr_port, expr_name):
    packets = rdpcap(pcap_path)
    sess_keys = packets.sessions()
    sess_keys_list = packets.sessions(sort_keys_to_list).keys()

    for k, v in sess_keys.items():
        data[k] = [None] * 2500
        time_stamp[k] = [None] * 2500
        seq_table[k] = [None] * 2500
    try:
        for k, v in sess_keys.items():
            ack[k] = 0
            for pack in v:
                ip_addr_src[k]=pack[IP].src
                ip_addr_dst[k]=pack[IP].dst
                port_src[k] = pack[TCP].sport
                port_dst[k]=pack[TCP].dport
                if pack[TCP].haslayer(Raw):
                    if str(method).lower() == 'first':
                        data[k], time_stamp[k], ack[k], seq_table[k] = first_no_overwrite(p=pack, data_list=data[k],
                                                                                      timestamp_list=time_stamp[k],
                                                                                      data_offset=pack[TCP].seq,
                                                                                      payload=pack[TCP].payload,
                                                                                      ack=ack[k], seq_list=seq_table[k], check=checksum)
                    else:
                        data[k], time_stamp[k], ack[k], seq_table[k] = last(p=pack, data_list=data[k],
                                                                                          timestamp_list=time_stamp[k],
                                                                                          data_offset=pack[TCP].seq,
                                                                                          payload=pack[TCP].payload,
                                                                                          ack=ack[k], seq_list=seq_table[k], check=checksum)
    except Exception:
        return
    for k, v in sess_keys.items():
        del (data[k][0])
        current_data = data[k]
        current_timestamp = time_stamp[k]
        seq_current = seq_table[k]
        current_data = [x for x in current_data if x is not None]
        current_timestamp = [x for x in current_timestamp if x is not None]
        seq_current = [x for x in seq_current if x is not None]
        data[k] = ''.join(current_data)
        # match = re.findall(expr, str(data[k]))
        for match in re.finditer(expr, str(data[k])):
            time = int(match.start())
            out = {"timestamp": current_timestamp[time], "source": {"ipv4_address": ip_addr_src[k], "tcp_port": port_src[k]}, "target":{"ipv4_address": ip_addr_dst[k], "tcp_port": port_dst[k]}, "rule": expr_name}
            print(yaml.dump(out, default_flow_style=False))


def main():
    path = sys.argv[1]

    try:
        with open(path) as config_file:
            data = yaml.load(config_file)
    except Exception:
        print("Couldn't find config file path")
        sys.exit(1)

    interface = data["interface"]
    path = data["pcap_path"]
    subnet = data["network"]
    checksum_enable = data["enable_checksums"]
    ip_reassembly_behaviour = data["ipv4_fragment_reassembly"]["default_behavior"]
    ip_endpoint_address = data["ipv4_fragment_reassembly"]["endpoints"][0]["ipv4_address"]
    ip_endpoint_behaviour = data["ipv4_fragment_reassembly"]["endpoints"][0]["behavior"]
    tcp_behaviour = data["tcp_reassembly"]["default_behavior"]
    tcp_endpoint_ipaddr = data["tcp_reassembly"]["endpoints"][0]["ipv4_address"]
    tcp_endpoint_behaviour = data["tcp_reassembly"]["endpoints"][0]["behavior"]
    rule_name = data["rules"][0]["name"]
    rule_destport = data["rules"][0]["destination_port"]
    # rule_content = bytes(data["rules"][0]["content"]).decode('utf-8', errors="ignore")
    rule_content = bytes(data["rules"][0]["destination_port"]).encode('ascii')
    # rule_content = data["rules"][0]["content"]

    if checksum_enable is None:
        print("Checksum entry not found")
        sys.exit(1)

    if ip_endpoint_behaviour and ip_reassembly_behaviour is None:
        print("IP Reassembly behaviour not found in config")
        sys.exit(1)

    if tcp_behaviour and tcp_endpoint_behaviour is None:
        print("TCP Stream Reassembly behaviour not found in config")
        sys.exit(1)

    if rule_content and rule_destport is None:
        print("Rule content Regex and port not found in config file")
        sys.exit(1)

    if ip_endpoint_behaviour is None:
        ip_endpoint_behaviour = ip_reassembly_behaviour

    if tcp_endpoint_behaviour is None:
        tcp_endpoint_behaviour = tcp_behaviour

    if subnet is None:
        print("Couldn't find network to be monitored in config file.")
        sys.exit(1)

    if interface and path is None:
        print("Couldn't find pcap file path or interface to be monitored")
        sys.exit(1)

    if path:
        try:
            ip_reassembly(path, ip_endpoint_behaviour, checksum_enable)
        except Exception:
            pass
        try:
            tcp_reassembly(path, tcp_endpoint_behaviour, checksum_enable, rule_content, rule_destport, rule_name)
        except Exception:
            pass


if __name__ == '__main__':
    main()
